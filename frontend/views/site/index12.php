<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
$name_colors = ['#2F4F4F', '#191970', '#0000CD', '#00CED1', '#006400', '#20B2AA', '#7CFC00', '#228B22', '#CD5C5C', '#FF0000', '#C71585', '#9400D3', '#8B795E' ];
?>
<div class="site-index">
    
    <div class="col-sm-12 col-md-12">
        <h1>Test chat!</h1>
    </div>     

    <div class="row">
        <div class="col-md-9 col-sm-9 messages-block border">
            
            <div class="messages col-sm-12" id="messages">
                    
                    
                
            </div>

            <div class="message-input">
                <form id="WebChatFormForm" class="form-horizontal" >
                  <div class="form-group">
                    <label class="control-label col-sm-1" for="comment">Message:</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" id="message" placeholder="Your message"></textarea>
                    </div>
                  </div>
                  <div class="form-group"> 
                    <div class="col-sm-offset-9 col-sm-3">
                      <button type="button" class="btn btn-success btn-lg" onclick="web_send_msg();">Submit</button>
                    </div>
                  </div>
                </form>
            </div>

        </div>
        <div class="col-md-3 users-block border">
            <div class="col-sm-12 col-md-12"><h3>Users in chat</h3></div>
            
            <?php for ($i=0; $i < 10 ; $i++) { ?>
                        
                <div class="col-md-12"><p>user nickname</p></div>

            <?php
            } ?>
            
        </div>
    </div>

</div>

<script>
         // Отправляет сообщение в чат
         function web_send_msg()
         {
             // Получение значений из элементов ввода.
             var text = $("#message").val(); // Получаем текст сообщения
             var name = '<?= Yii::$app->user->identity->username ?>'; // Получаем имя пользователя
 
             // Очистка поля с текстом сообщения
             $("#message").val("");  
 
             // Добавление отправленного сообщения к списку сообщений.
             $("#messages").append(
                '<div class="row message" style="color:<?= $name_colors[rand(0, count($name_colors) - 1)]?>"><div class="col-sm-2 name">'+name+'</div><div class="col-sm-10">'+text+'</div></div>'
                );
 
             // Отправка сообщения в канал чата
             CometServer().web_pipe_send("web_chat_pipe", {"text":text, "name":name});
         }
 
 
         // Функция выполнится после загрузки страницы
         $(document).ready(function()
         {
             CometServer().start({dev_id:1}) // Подключаемся к комет серверу
 
             // Подписываемся на канал в который и будут отправляться сообщения чата.
             CometServer().subscription("web_chat_pipe", function(msg)
             {
                console.log(msg)
                 // Добавление полученного сообщения к списку сообщений.
                 $("#messages").append(
                    '<div class="row message" style="color:<?= $name_colors[rand(0, count($name_colors) - 1)]?>"><div class="col-sm-2 name">Nickname :'+msg.data.name+'</div><div class="col-sm-10">'+msg.data.text+'</div></div>'
                    );
             });
 
             // Подписываемся на канал в который и будут отправляться уведомления о доставке отправленных сообщений.
             /*CometServer().subscription("#web_chat_pipe", function(p)
             {
                console.log(p)
                 $("#messages").html("Сообщение доставлено "+p.data.number_messages+" получателям. "+p.data.error);
             });*/
         });
    </script>

    


