<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
$colors = ['#2F4F4F', '#191970', '#0000CD', '#00CED1', '#006400', '#20B2AA', '#7CFC00', '#228B22', '#CD5C5C', '#FF0000', '#C71585', '#9400D3', '#8B795E' ];
$color = $colors[rand(0, count($colors)-1)];
?>

<div class="site-index">
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h1>Test chat!</h1>  
        </div>
        <div id="user-container">   
            <button type="button" class="btn btn-primary btn-lg" id="join-chat">Go Chat</button> 
        </div> 
    </div>

    <div class="row hidden" id="main-container"> 

        <div class="col-md-9 col-sm-9 col-xs-9 messages-block border">
            
            <div class="row">        
                <button type="button" class="btn btn-warning btn-lg" id="leave-room" style="margin-bottom: 10px;" >Leave chat</button>
                <div id="messages" class="messages col-sm-12">
                    
                </div>

                <div id="msg-container">
                    <form id="WebChatFormForm" class="form-horizontal" >
                      <div class="form-group">
                        <label class="control-label col-sm-1" for="comment">Message:</label>
                        <div class="col-sm-10 col-xs-12">
                            <textarea class="form-control" rows="5" id="msg" name="msg" maxlength="200" placeholder="Enter your message"></textarea>
                        </div>
                      </div>
                      <div class="form-group"> 
                        <div class="col-sm-offset-8 col-sm-2 col-xs-6">
                          <button type="button" id="send-msg" class="btn btn-success btn-lg">Send message</button>
                        </div>
                      </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-sm-3 col-xs-3 users-block border">
            <h3 class="text-center">Users in chat<hr></h3>
            <div class="row" id="users">
                
            </div>
            
        </div>
    </div>

</div>

<script id="messages-template" type="text/x-handlebars-template">
    {{#each messages}}
    <div class="msg col-sm-12 col-xs-12">
        <div class="details row" style="color: {{color_mes}}">
            <span class="user col-sm-2">{{user}}:</span><span class="text col-sm-8">{{text}}</span>
            <span class="time col-sm-2">{{time}}</span>
        </div>
    </div>
    {{/each}}
</script>
<script id="users-template" type="text/x-handlebars-template">
    {{#each users}}
    <div class="col-sm-12 col-xs-12" id="nick-{{id}}" style="color:{{color}}"><p>{{nick}}</p></div>
    {{/each}}
</script>

<script>
    var user_name = '<?= Yii::$app->user->identity->username ?>';
    var user_color = '<?=$color?>';
</script>
<script>
    <?php if ( Yii::$app->user->identity->username == 'admin' && Yii::$app->user->identity->id == 1) : ?>
        var isAdmin = true;
        <?php else : ?>
        isAdmin = false;
    <?php endif; ?>
</script>


<script src="js/main.js"></script>


    


