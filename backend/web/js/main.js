(function(){

    var user;
    var time = 0;
    var messages = [];
    var color = getRandomColor();

    var messages_template = Handlebars.compile($('#messages-template').html());
    var users_template = Handlebars.compile($('#users-template').html());

    function updateMessages(msg){
        messages.push(msg);
        var messages_html = messages_template({'messages': messages});
        $('#messages').html(messages_html);
        $("#messages").animate({ scrollTop: $('#messages')[0].scrollHeight}, 1000);
    }

    var conn = new WebSocket('ws://localhost:8080');
    conn.onopen = function(e) {
        console.log("Connection established!");
    };
    conn.onerror = function(e) {
        console.log('Error');
    };

    conn.onmessage = function(e) {
        var msg = JSON.parse(e.data);

        switch (msg.type){
            case 'message':
                updateMessages(msg);
                break;

            case 'new_user':
                updateMessages(msg);
                updateUsersList(msg.users);
                console.log(msg.users);
                break; 

            case 'hello':
				usersList(msg.text);
				break;     

            case 'ban':
            	console.log(msg.type);
            	break;
            case 'unban':
                updateMessages(msg);
                break;      
        } 
    };

    function usersList(e){
        var users = JSON.parse(e);
        var user_html = users_template({'users': users});
        $('#users').html(user_html);
    }

    function updateUsersList(e){
        var users = JSON.parse(e);
        $('#users > *').remove();
        var user_html = users_template({'users': users});
        $('#users').html(user_html);
    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    $('#join-chat').click(function(){
        user = user_name;
        $('#user-container').addClass('hidden');
        $('#main-container').removeClass('hidden');
        
        var msg = {
            "type": "new_user",
            "user": user,
            "text": user + " entered the room",
            "time": moment().format('hh:mm a'),
            "color_mes": color,
        };

        updateMessages(msg);
        conn.send(JSON.stringify(msg));
    });

    $('#bun-btn').click(function(){
        var bun_user = $('#bun').val();

        var msg = {
            "type": "ban",
            "user": user,
            "text": bun_user + " was banned!",
            "time": moment().format('hh:mm a'),
            "user_id": bun_user,
        };
        
        console.log(msg);
        updateMessages(msg);
        conn.send(JSON.stringify(msg));
        var bun_user = $('#bun').val('');
    });

    $('#unbun-btn').click(function(){
        var unbun_user = $('#unbun').val();

        var msg = {
            "type": "unban",
            "user": user,
            "text": unbun_user + " was unbanned!",
            "time": moment().format('hh:mm a'),
            "user_id": unbun_user,
        };
        
        console.log(msg);
        updateMessages(msg);
        conn.send(JSON.stringify(msg));
        $('#unbun').val('');
    });

    $('#send-msg').click(function(){

        if (time > Date.now()) {
            $('#msg').val('');
            $('#msg').attr("placeholder", "Not so fast!");
        } else {
            var text = $('#msg').val();
            $('#msg').val('');
            var msg = {
                "type":'message',
                "user": user,
                "text": text,
                "time": moment().format('hh:mm a'),
                "color_mes": color,
            };
            updateMessages(msg);
            conn.send(JSON.stringify(msg));
            $('#msg').attr("placeholder", "Enter your message");
        }

        time = Date.now() + 15000;

    });

    $('#leave-room').click(function(){
        var msg = {
            "type":"user_del",
            "user": user,
            "text": user + " has left the room",
            "time": moment().format('hh:mm a'),
        };
        updateMessages(msg);
        conn.send(JSON.stringify(msg));

        $('#messages').html('');
        messages = [];

        $('#nick-' + user).remove();
        $('#main-container').addClass('hidden');
        $('#user-container').removeClass('hidden');
        $('#users > *').remove();
    });

})();