<?php

namespace console\controllers;

use Yii;
use yii\helpers\Json;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\helpers\ArrayHelper;

class Chat implements MessageComponentInterface 
{

	private $clients = [];

	public $users = [];

	public function __construct() 
	{
    	$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn) 
	{
	    //store the new connection
	    $this->clients->attach($conn);

	    Yii::info('Connection is established', 'chat');
	    echo "New connection! ({$conn->resourceId})\n";
	}

	public function onMessage(ConnectionInterface $from, $msg) 
	{
	    //send the message to all the other clients except the one who sent.
		$msg = json_decode($msg);

		var_dump($msg);
		var_dump(time());

		if (!is_object($msg) || !isset($msg->type)){
			return false;
		}
		
		switch($msg->type) {
			case 'message':

				if ($this->users[$from->resourceId]['status'] === 'unban') {
					
					if ($this->users[$from->resourceId]['interval'] > time()){
						$mes = [
							'type'=>'ban',
							'text'=>'You have ban',
							'user'=>'admin',
							'time'=>$msg->time,
						];
						$mes = json_encode($mes);
						$from->send($mes);	
					} else {
						$this->sendToClients( $from, [
							'type'=>'message',
							'text'=>$msg->text,
							'user'=>$msg->user,
							'time'=>$msg->time,
							'color_mes'=>$msg->color_mes,
						]);
						$this->users[$from->resourceId]['interval'] = time() + 15;
					}
					
				} else {
					$answer = [
						'type' => 'ban',
						'text' => 'You have ban',
						'user' => 'admin',
						'time'=>$msg->time,
					];
					$from->send(json_encode($answer));
				}
				
				break;

			case 'ban':				
					if (isset($this->users[$msg->user_id])) {
						$this->sendBun($msg->user_id);
					}
					var_dump($msg->type);
				break;

			case 'unban':
				if (isset($this->users[$msg->user_id])) {
					$this->sendToClients( $from, [
						'type'=>'unban',
						'text'=>'unban'.' - '.$this->users[$msg->user_id]['nick'],
						'user'=>'admin',
						'time'=>$msg->time,
					]);
					$this->users[$msg->user_id]['status'] = 'unban';	
				}
				break;
			
			case 'new_user':
					$this->checkNewUser($msg, $from->resourceId);
				var_dump($this->users);
				$data = [
					'type' => 'new_user',
					'text' => $msg->text,
					'user' => $msg->user,
					'time' => $msg->time,
					'users' => $this->activeUsers(),
				];
				$this->sendToClients($from, $data);	
				$answer = [
					'type'=>'hello',
					'text' => $this->activeUsers(),
					'user' => $msg->user,
					'time' => $msg->time,
				];	
				$from->send(json_encode($answer));
				if ($this->users[$from->resourceId]['status'] === 'ban') {
					$answer = [
						'type'=>'ban',
						'text' => 'You have ban',
						'user' => 'admin',
						'time' => $msg->time,
					];	
				$from->send(json_encode($answer));
				}
				break;
		}
	}

	protected function checkNewUser($msg, $id) 
	{
		$a = false; 
		foreach ($this->users as $user) {
			if (in_array($msg->user, $user)) {
				$this->users[$id] = $user;
				$a = true;
			} else $a = false;
		}
		if (!$a) {
			$this->users[$id] = [
				'id' => $id,
				'nick' => $msg->user,
				'color' => $msg->color_mes,
				'interval' => time(),
				'status' => 'unban',
			];
		}
		
	}

	protected function sendBun($id)
	{
		$this->users[$id]['status'] = 'ban';
	}

	/* this function return array of clients connections id */
	protected function getClients() 
	{
		$clients_id = [];
		foreach ($this->clients as $client) {
			array_push($clients_id, $client->resourceId);
		}
		return $clients_id;
	}

	/* This function return list of active users */
	protected function activeUsers()
	{
		$i = true;
		$usersOnline = [];
		foreach ($this->users as $user) {
			if (in_array($user['id'], $this->getClients())) {
				if ($i !== $user['id']) {
					array_push($usersOnline, $user);
					$i = $user['id'];
				}
			}
		}
		return json_encode($usersOnline);
	}

	protected function sendToClients($from, $data)
	{
		foreach ($this->clients as $client) {
	        if ($from !== $client) {
	            $client->send(json_encode($data));
	        }
	    }	
	}

	protected function isAdmin($msg)
	{
		if ($msg === 'admin') {
			return true;
		}
	}

	public function onClose(ConnectionInterface $conn) 
	{
	    $this->clients->detach($conn);
	    Yii::info('Connection is closed', 'chat');
	    echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e) 
	{
	    Yii::error($e->getMessage());
	    $conn->send(Json::encode(['type' => 'error', 'data' => [
            'message' => Yii::t('app', 'Something wrong. Connection will be closed')
        ]]));
        echo "An error has occurred: {$e->getMessage()}\n";
	    $conn->close();
	}


}