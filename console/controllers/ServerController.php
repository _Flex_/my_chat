<?php 

namespace console\controllers;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use console\controllers\Chat;

class ServerController extends \yii\console\Controller
{
    public function actionRun()
    {
        $server = IoServer::factory(
		    new HttpServer(
		        new WsServer(
		            new Chat()
		        )
		    ),
		    8080
		);
        $server->run();
        // echo 'Server was started successfully. Setup logging to get more details.'.PHP_EOL;
    }
}
