<?php

use yii\db\Schema;
use yii\db\Migration;

class m160411_143453_create_admin_profile extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'username' => 'root',
            'email' => 'admin@mail.com',
            'password_hash' => md5('123456'),
            'created_at' => time()
        ]);
    }

    public function down()
    {
        $this->delete('user', ['username' => 'root']);
    }
}
